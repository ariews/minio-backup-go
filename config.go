package main

import (
	"os"

	"github.com/joho/godotenv"
)

type config struct {
	Source    string
	Bucket    string
	Prefix    string
	AccessKey string
	SecretKey string
	EndPoint  string
}

func loadConfig(name string) (*config, error) {
	err := godotenv.Load(name)
	if err != nil {
		return nil, err
	}

	return &config{
		Source:    os.Getenv("TARGET"),
		Bucket:    os.Getenv("MINIO_BUCKET_NAME"),
		Prefix:    os.Getenv("MINIO_PATH_PREFIX"),
		AccessKey: os.Getenv("MINIO_ACCESS_KEY"),
		SecretKey: os.Getenv("MINIO_SECRET_KEY"),
		EndPoint:  os.Getenv("MINIO_ENDPOINT"),
	}, nil
}
