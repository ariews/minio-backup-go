package main

import (
	"io/fs"
	"log"
	"path/filepath"
	"regexp"
)

var (
	fileMatcher = regexp.MustCompile(".bz2$")
	configFile  = "config"
)

func fileWalker(m *minioInstance) fs.WalkDirFunc {
	return func(filepath string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if !d.IsDir() && fileMatcher.MatchString(d.Name()) {
			if e := m.Upload(filepath, d); e != nil {
				log.Printf("[SKIPPED] %s: %s", d.Name(), e.Error())
			} else {
				log.Printf("[UPLOADED] %s", d.Name())
			}
		}
		return nil
	}
}

func main() {
	c, e := loadConfig(configFile)
	if e != nil {
		log.Fatal(e)
	}

	m, e := createMinioInstance(c)
	if e != nil {
		log.Fatal(e)
	}

	e = filepath.WalkDir(c.Source, fileWalker(m))
	if e != nil {
		log.Fatal(e)
	}
}
