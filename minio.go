package main

import (
	"context"
	"errors"
	"io/fs"
	"os"
	"strings"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

type minioInstance struct {
	client *minio.Client
	config *config
}

func (m minioInstance) isFileExists(name string) bool {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	_, err := m.client.StatObject(ctx, m.config.Bucket, name, minio.StatObjectOptions{})

	// assume every error means file not exists
	return err == nil
}

func (m minioInstance) Upload(filepath string, d fs.DirEntry) error {
	keyName := strings.ReplaceAll(filepath, m.config.Source, m.config.Prefix)
	if m.isFileExists(keyName) {
		return errors.New("target exists")
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	file, err := os.Open(filepath)
	if err != nil {
		return err
	}

	fifo, err := d.Info()
	if err != nil {
		return err
	}

	_, err = m.client.PutObject(ctx, m.config.Bucket, keyName, file, fifo.Size(), minio.PutObjectOptions{})

	return err
}

func createMinioInstance(c *config) (*minioInstance, error) {
	mc, err := minio.New(c.EndPoint, &minio.Options{
		Creds: credentials.NewStaticV4(c.AccessKey, c.SecretKey, ""),
	})

	if err != nil {
		return nil, err
	}

	return &minioInstance{client: mc, config: c}, nil
}
